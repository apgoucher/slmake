#pragma once

#include "../lifelib/pattern2.h"
#include "../lifelib/spantree.h"
#include "../lifelib/classifier.h"
#include "../lifelib/ssplit.h"
#include <set>
#include <cstdlib>
#include <sstream>
#include <algorithm>

#define MIN_EXCLUSION_SIZE 32

/*
* Functionality for constructing constellations of still-lifes.
*/

namespace apg {

    /**
     * Computes the period-8 envelope of a pattern
     */
    pattern to_env(pattern x) {

        pattern y = x;
        pattern z = x;

        for (int i = 0; i < 7; i++) {
            z = z[1];
            y += z;
        }

        return y;
    }

    double stlength(pattern stell, classifier &cfier) {

        bitworld env = to_env(stell).flatlayer(0);
        bitworld live = stell.flatlayer(0);

        std::vector<bitworld> clusters = cfier.getclusters(live, env, false);
        std::vector<coords64> ccentres;

        for (uint64_t i = 0; i < clusters.size(); i++) {

            // Get bounding box:
            int64_t bbox[4] = {0};
            clusters[i].getbbox(bbox);
            int64_t mx2 = bbox[0] * 2 + bbox[2] - 1;
            int64_t my2 = bbox[1] * 2 + bbox[3] - 1;
            coords64 m2(mx2, my2);
            ccentres.push_back(m2);

        }

        return 0.5 * spanning_tree_length(ccentres);

    }

    pattern diagonalise(pattern inp) {
        /*
        * Create a diagonal line longer than the diameter of a pattern.
        */

        int64_t bbox[4] = {0};
        inp.getrect(bbox);

        pattern diagonal(inp.getlab(), "o$bo$2bo$3bo$4bo$5bo$6bo$7bo$8bo$9bo$10bo$11bo$12bo$13bo$14bo$15bo!", inp.getrule());
        for (uint64_t i = 4; i < 64; i++) {
            if (((1 << i) >= bbox[2]) && ((1 << i) >= bbox[3])) { break; }
            diagonal += diagonal(1 << i, 1 << i);
        }

        return diagonal;
    }

    pattern get_exclusions(pattern inpat, classifier &cfier) {

        auto lab = inpat.getlab();

        pattern excl(lab, "", "b3s23");

        bitworld live = inpat.flatlayer(0);
        std::vector<bitworld> clusters = cfier.getclusters(live, live, true);
        for (uint64_t i = 0; i < clusters.size(); i++) {
            if (clusters[i].population() >= MIN_EXCLUSION_SIZE) {
                excl += pattern(lab, lab->demorton(clusters[i], 1), "b3s23");
            }
        }

        return excl;
    }

    uint64_t excluded_popcount(pattern inpat, classifier &cfier) {
        pattern excl = get_exclusions(inpat, cfier);
        return (inpat - excl).popcount((1 << 30) + 3);
    }

    pattern cell_lowerright(pattern inpat) {
        // Get a reasonably lower-right cell of an input pattern.
        bitworld bw = inpat.flatlayer(0);
        bw = bw.br1cell();
        auto lab = inpat.getlab();
        pattern brcell(lab, lab->demorton(bw, 1), inpat.getrule());
        return brcell;
    }

    pattern bounding_hull(pattern x) {

        int64_t bbox[4] = {0};
        x.getrect(bbox);

        pattern y(x.getlab(), "bbo$bo$o!", "b3s23");
        y = y.shift(-1, -1);

        uint64_t p = bbox[2] + bbox[3];
        while (p > 0) {
            p = p >> 1;
            y = y.convolve(y);
        }

        return x.convolve(y).subrect(bbox);
    }

    pattern get_isolated_block(pattern inpat) {

        auto lab = inpat.getlab();
        pattern convrect(lab, lab->rectangle(-20, -20, 41, 41), "b3s23");
        return inpat & cell_lowerright(inpat).convolve(convrect);

    }

    bool has_isolated_block(pattern inpat) {

        pattern x = get_isolated_block(inpat);
        int64_t bbox[4] = {0};
        x.getrect(bbox);
        return (bbox[2] == 2) && (bbox[3] == 2);

    }

    pattern get_lowerright(pattern inpat, pattern inpat2, uint64_t minpop, int radius, classifier &cfier) {
        // Get a lower-right chunk of an input pattern.
        auto lab = inpat.getlab();
        pattern convrect(lab, lab->rectangle(-radius, -radius, 2 * radius + 1, 2 * radius + 1), "b3s23");

        pattern icell = cell_lowerright(inpat2);

        pattern diag = diagonalise(inpat);

        pattern sword(lab, "o$bo$2bo$3b2o$3b2o!", "b3s23");

        for (int i = 0; i < 6; i++) {
            sword = sword.convolve(sword);
        }

        sword = sword.convolve(diag).convolve(convrect);

        pattern chunk(lab, "", "b3s23");

        for (;;) {
            pattern newchunk = chunk + icell;

            while (chunk != newchunk) {
                chunk = newchunk & inpat;
                newchunk = bounding_hull(chunk).convolve(sword) & inpat;
            }

            pattern rempat = inpat - chunk;

            if (rempat.empty()) { break; }

            uint64_t epc = excluded_popcount(chunk, cfier);
            if (epc < minpop) {
                icell = icell.convolve(convrect);
            } else {
                std::cout << "\033[36;1mFound " << epc;
                std::cout << "-cell chunk; " << excluded_popcount(rempat, cfier);
                std::cout << " cells remain.\033[0m" << std::endl;
                break;
            }
        }
        return chunk;
    }

    std::vector<pattern> dimerise(pattern stell, classifier &cfier) {
        /*
        * Find promising still-life pairs and individual still-lifes
        * which the compiler has a good chance of being able to build.
        */

        std::vector<pattern> dimers;
        // std::vector<std::vector<pattern> > sdimers;

        bitworld env = to_env(stell).flatlayer(0);
        bitworld live = stell.flatlayer(0);

        std::vector<bitworld> clusters = cfier.getclusters(live, env, true);
        std::vector<coords64> ccentres;

        std::set<std::pair<int64_t, edge64>> edgelist_sorted;

        for (uint64_t i = 0; i < clusters.size(); i++) {
            // Get bounding box:
            int64_t bbox[4] = {0};
            clusters[i].getbbox(bbox);
            int64_t mx2 = bbox[0] * 2 + bbox[2] - 1;
            int64_t my2 = bbox[1] * 2 + bbox[3] - 1;
            coords64 m2(mx2, my2);
            ccentres.push_back(m2);

            // penalty to ensure that singleton blocks appear after everything else:
            int64_t blockpen = ((bbox[2] == 2) && (bbox[3] == 2)) ? 480 : 80;

            edgelist_sorted.emplace(blockpen - 2*mx2 - 2*my2, edge64(i, i));
        }

        std::vector<edge64> edgelist_unsorted = spanning_graph(ccentres);

        for (uint64_t i = 0; i < edgelist_unsorted.size(); i++) {
            edge64 edge = edgelist_unsorted[i];

            int64_t x1 = ccentres[edge.first].first;
            int64_t y1 = ccentres[edge.first].second;
            int64_t x2 = ccentres[edge.second].first;
            int64_t y2 = ccentres[edge.second].second;

            if ((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2) > 5000) { continue; }

            edgelist_sorted.emplace(0 - (x1 + y1 + x2 + y2), edge);
        }

        lifetree_abstract<uint32_t>* lab = stell.getlab();

        std::set<edge64> edgedump;

        for (auto it = edgelist_sorted.begin(); it != edgelist_sorted.end(); ++it) {

            edge64 edge = it->second;

            if (edge.first == edge.second) {
                // Include monomers:
                uint64_t i = edge.first;
                pattern monomer(lab, lab->demorton(clusters[i], 1), stell.getrule());
                dimers.push_back(monomer);
            } else {
                // Include dimers:
                uint64_t idx = (edge.first < edge.second) ? edge.first : edge.second;
                uint64_t idy = (edge.first > edge.second) ? edge.first : edge.second;
                bitworld bw = clusters[edge.first];
                bw += clusters[edge.second];
                pattern dimer(lab, lab->demorton(bw, 1), stell.getrule());
                edge64 newedge(idx, idy);
                if (edgedump.count(newedge) == 0) {
                    dimers.push_back(dimer);
                    edgedump.insert(newedge);
                }
            }
        }

        return dimers;
    }

    template <typename T> struct cgsalvo {

        std::vector<std::pair<T, char> > gliders;
        T dx;
        T dy;
        bool age;
        bool transpose;

        void glidermatch(pattern pat) {

            std::map<std::pair<int64_t, int64_t>, uint8_t> gmap;
            std::vector<pattern> matches;

            pattern a_glider(pat.getlab(), "3o$o$bo!", pat.getrule());

            int64_t bbox[4] = {0};

            for (uint64_t i = 0; i < 4; i++) {
                bitworld bw = pat.match(a_glider).flatlayer(0);
                a_glider = a_glider[1];

                while (bw.population()) {
                    bitworld onecell = bw.get1cell();
                    bw -= onecell;
                    onecell.getbbox(bbox);
                    int64_t lane = (bbox[0] - bbox[1]);
                    uint8_t phase = (uint8_t) (((bbox[1] & 1) << 2) + i);
                    gmap[std::pair<int64_t, int64_t>(bbox[0] + bbox[1], lane)] = phase;
                }
            }

            for (auto it = gmap.begin(); it != gmap.end(); ++it) {
                T lane = it->first.second;
                uint8_t phase = it->second;
                gliders.emplace_back(lane, phase);
            }
        }

        std::pair<pattern, pattern> frompattern(pattern pat) {

            int64_t bbox[4] = {0};
            pattern fin = pat[1 << 20];
            
            to_env(fin).getrect(bbox);
            pattern centred = pat(-bbox[0], -bbox[1]);
            fin = fin(-bbox[0], -bbox[1]);

            pattern target = centred & centred[8];
            pattern gliders = centred - target;

            glidermatch(gliders);

            dx = 0;
            dy = 0;
            age = 0;
            transpose = 0;

            return std::pair<pattern, pattern>(target, fin);
        }

        void fromline(std::string line) {
            std::vector<std::string> colons = string_split(line, ':');
            if (colons.size() >= 3) {

                std::string t = colons[colons.size() - 2];
                std::vector<std::string> gstrings = string_split(t, ' ');
                for (uint64_t i = 0; i < gstrings.size(); i++) {
                    std::string g = gstrings[i];
                    if (g != "") {
                        char lastchar = g[g.length() - 1];
                        if ((lastchar == 'E') || (lastchar == 'O')) {
                            T j = (std::stoll(g.substr(0, g.length() - 1)) - 1) / 2;
                            gliders.emplace_back(j, ((lastchar == 'E') ? 0 : 1));
                        }
                    }
                }

                std::string s = colons[colons.size() - 3];
                std::replace(s.begin(), s.end(), '(', ' ');
                std::replace(s.begin(), s.end(), ')', ' ');
                std::replace(s.begin(), s.end(), ',', ' ');
                std::vector<std::string> hstrings = string_split(s, ' ');
                std::vector<std::string> hstrings2;
                for (uint64_t i = 0; i < hstrings.size(); i++) {
                    std::string h = hstrings[i];
                    if (h != "") { hstrings2.push_back(h); }
                }
                if (hstrings2.size() == 3) {
                    dx = std::stoll(hstrings2[0]);
                    dy = std::stoll(hstrings2[1]);
                    transpose = (hstrings2[2] == "T");
                }

                std::string r = colons[colons.size() - 1];
                if (r.find("o") != std::string::npos) { age = 1; }
                if (r.find("e") != std::string::npos) { age = 0; }

            }
        }

    };

    struct cgfile {
        std::map<std::string, std::vector<std::vector<std::pair<std::string, cgsalvo<int16_t>>>>> sdata;
        std::set<std::string> btargets;

        void digestmc(std::string filename, lifetree_abstract<uint32_t> *lab) {

            pattern x(lab, filename);
            uint64_t period = x[1 << 20].ascertain_period();
            std::cout << " -- " << filename << " has period " << period << "." << std::endl;

            for (uint64_t i = 0; i < period; i++) {
                for (uint64_t j = 0; j < 2; j++) {
                    cgsalvo<int16_t> cg;
                    std::pair<pattern, pattern> respair = cg.frompattern(x);
                    std::string source = respair.first._string32();
                    std::string target = respair.second._string32();
                    btargets.insert(target);

                    if (sdata[target].size() <= cg.gliders.size()) {
                        sdata[target].resize(cg.gliders.size() + 1);
                    }
                    sdata[target][cg.gliders.size()].emplace_back(source, cg);

                    x = x.transpose();
                }
                x = x[1];
            }
        }

        void readfile(std::string filename, lifetree_abstract<uint32_t> *lab, std::string rule) {
            std::ifstream f(filename);
            std::string line;
            std::string rlesofar;
            std::string target;
            std::string source;
            bool readingsrc = false;

            if (!f.good()) { return; }

            std::cout << "Reading file " << filename << "..." << std::endl;
            while (std::getline(f, line)) {
                if (line.empty()) { continue; }
                char c = line[0];

                if ((c == ' ') || (c == '*')) {
                    if (rlesofar != "") { rlesofar += "$"; }
                    rlesofar += line;
                } else if (rlesofar != "") {
                    std::replace( rlesofar.begin(), rlesofar.end(), ' ', 'b');
                    std::replace( rlesofar.begin(), rlesofar.end(), '*', 'o');
                    rlesofar += "!";
                    // std::cout << rlesofar << std::endl;
                    pattern p(lab, rlesofar, rule);
                    if (readingsrc) { source = p._string32(); } else { target = p._string32(); }
                    rlesofar = "";
                }

                if (c == '-') {
                    if (line.find("Source") != std::string::npos) {
                        readingsrc = true;
                    } else if (line.find("Target") != std::string::npos) {
                        readingsrc = false;
                    }
                } else if (rlesofar == "") {
                    cgsalvo<int16_t> cg;
                    cg.fromline(line);
                    if (cg.gliders.size() != 0) {
                        if (sdata[target].size() <= cg.gliders.size()) {
                            sdata[target].resize(cg.gliders.size() + 1);
                        }
                        sdata[target][cg.gliders.size()].emplace_back(source, cg);
                    }
                }
            }
            std::cout << "..." << filename << " successfully read." << std::endl;
        }
    };

    struct cghq {
        /*
        * Collection of slow-salvo recipes
        */

        std::map<std::string, cgfile> cgfiles;
        std::string rule;
        std::string datadir;
        lifetree_abstract<uint32_t> *lab;

        uint64_t assertfile(std::string filename) {
            if (cgfiles.count(filename) == 0) {
                cgfiles[filename].readfile(filename, lab, rule);
            }
            return cgfiles[filename].sdata.size();
        }

        void assertbespoke(std::string dirname) {
            if (cgfiles.count(dirname) == 0) {
                std::ifstream f(dirname + "/filelist.txt");
                std::string line;
                while (std::getline(f, line)) {
                    std::string filename = dirname + "/" + line;
                    cgfiles[dirname].digestmc(filename, lab);
                }
            }
        }

        pattern precurse(pattern orig, classifier &cfier, int state, int initbail, int maxbail, uint32_t lastbail, int64_t *ideal) {

            lab = orig.getlab();
            rule = orig.getrule();

            assertbespoke(datadir+"bespoke");

            pattern stell = orig & orig[8];
            pattern exsalvo = orig - stell;
            pattern diagonal = diagonalise(stell);
            pattern smallblock(lab, "2o$2o!", rule);
            pattern bigblock(lab, "4o$4o$4o$4o!", rule);

            std::vector<pattern> xdimers = dimerise(stell, cfier);

            pattern sword = diagonal.convolve(bigblock);

            std::vector<pattern> dimers;
            pattern avoid(lab, "", rule);

            for (auto it = cgfiles[datadir+"bespoke"].btargets.begin(); it != cgfiles[datadir+"bespoke"].btargets.end(); ++it) {
                pattern sterm(lab, lab->_string32(*it), rule);
                bitworld bw = stell.match(sterm).flatlayer(0);
                while (bw.population()) {
                    int64_t bbox[4] = {0};
                    bitworld onecell = bw.get1cell();
                    bw -= onecell;
                    onecell.getbbox(bbox);
                    pattern subset = sterm(bbox[0], bbox[1]);
                    avoid += subset;
                    dimers.push_back(subset);
                }
            }

            double stelllength = 0.0;

            uint64_t nbespoke = dimers.size();

            for (uint64_t i = 0; i < xdimers.size(); i++) {
                if ((xdimers[i] & avoid).empty()) {
                    dimers.push_back(xdimers[i]);
                }
            }

            uint64_t smallobj = 0;

            if (state != 0) {
                stelllength = stlength(stell - avoid, cfier);

                std::cout << "Obtained " << dimers.size() << " dimers/monomers";
                if (nbespoke) { std::cout << " (including " << nbespoke << " bespoke objects)"; }
                std::cout << "." << std::endl;

                bitworld env = to_env(stell).flatlayer(0);
                bitworld live = stell.flatlayer(0);
                std::vector<bitworld> clusters = cfier.getclusters(live, env, true);
                for (uint64_t i = 0; i < clusters.size(); i++) {
                    if (clusters[i].population() < MIN_EXCLUSION_SIZE) { smallobj += 1; }
                }
                std::cout << smallobj << " objects of < " << MIN_EXCLUSION_SIZE << " cells." << std::endl;
            }
            pattern eglider(lab, "3o$o$bo!", rule);

            int budget = nbespoke + 10;
            if (budget < 30) { budget = 30; }

            // Display dimers:
            for (uint64_t i = 0; i < dimers.size(); i++) {
                pattern dimer = dimers[i];
                bitworld live = dimer.flatlayer(0);
                bitworld env = to_env(dimer).flatlayer(0);
                std::map<std::string, int64_t> counts = cfier.census(live, env);

                std::ostringstream ss;
                uint64_t totobj = 0;
                for (auto it = counts.begin(); it != counts.end(); ++it) {
                    if (totobj != 0) { ss << "__"; }
                    if (it->second != 0) {
                        ss << it->first;
                        if (it->second != 1) { ss << "(" << it->second << ")"; }
                    }
                    totobj += it->second;
                }

                std::string lexrepr = ss.str();
                std::vector<std::string> prefices;

                prefices.push_back("bespoke");

                bool oneblock = false;

                if (lexrepr == "xs4_33") {
                    if (state == 0) { continue; }
                    if (smallobj == 1) { oneblock = true; }
                    prefices.push_back("longmove");
                } else if ((lexrepr == "xs6_696") || (lexrepr == "xs4_252")) {
                    prefices.push_back("edgy/xs4_33");
                } else {
                    prefices.push_back("edgy/xs4_33");
                    if (totobj == 1) {
                        prefices.push_back("edgy/xs6_696");
                        prefices.push_back("edgy/xs4_252");
                    }

                    budget -= 1;
                    if ((state == 0) && (budget == 0)) { break; }
                    if ((state != 0) && (budget > 0)) { continue; }
                }

                uint64_t bdiff = 0;
                if (oneblock) {
                    if (ideal != 0) {
                        int64_t bbox2[4] = {0};
                        dimer.getrect(bbox2);
                        bbox2[0] -= ideal[0];
                        bbox2[1] -= ideal[1];
                        bdiff = (bbox2[0] * bbox2[0]) + (bbox2[1] * bbox2[1]);
                    }

                    if (bdiff == 0) {
                        std::cout << "\033[32;1mAlgorithm terminated with single block.\033[0m" << std::endl;
                        return orig;
                    }
                }

                for (uint64_t z = 0; z < prefices.size(); z++) {

                  bool is_bespoke = (prefices[z] == "bespoke");

                  std::string filename = datadir + prefices[z];
                  if (!is_bespoke) {
                      filename += ("/" + lexrepr);
                  }
                  assertfile(filename);

                  int64_t bbox[4] = {0};
                  env.getbbox(bbox);

                  pattern remainder = stell - dimer;
                  pattern dcs = dimer.convolve(sword);

                  if ((dcs & remainder).nonempty() && (dcs(12, 0) & remainder).nonempty() && (dcs(0, 12) & remainder).nonempty()) { continue; }

                  for (uint64_t j = 0; j < (is_bespoke ? 1 : 4); j++) {

                    pattern tlt = dimer.shift(-bbox[0], -bbox[1]);
                    if (j & 1) {
                        if (tlt == tlt[1]) { continue; }
                        tlt = tlt[1];
                    }
                    if (j & 2) {
                        if (lexrepr == "xs4_33") { continue; }
                        tlt = tlt.transpose();
                    }
                    auto it = cgfiles[filename].sdata.find(tlt._string32());
                    if (it != cgfiles[filename].sdata.end()) {

                        uint64_t deep_bailout = initbail - 1;
                        uint64_t tree_bailout = initbail * 100;

                        uint64_t trycount = 0;

                        for (uint64_t k = 1; k < it->second.size(); k++) {

                            uint64_t n_recipes = it->second[k].size();

                            if (lexrepr == "xs4_33") {
                                if (trycount > tree_bailout) { break; }
                                std::cout << "Attempting to construct xs4_33 with " << k << " gliders (" << n_recipes << " recipes)" << std::endl;
                            }

                            for (uint64_t l = 0; l < n_recipes; l++) {

                                cgsalvo<int16_t> cs = it->second[k][l].second;
                                std::string srcstr = it->second[k][l].first;
                                pattern source(lab, lab->_string32(srcstr), rule);

                                // Determine whether it is worth proceeding:
                                bool trythis = false;
                                pattern xlt = source.shift(-cs.dx, -cs.dy);
                                pattern altstell = remainder + xlt.shift(bbox[0], bbox[1]);
                                double altstelllength = stelllength;
                                uint64_t altbdiff = 0;

                                bool oneblock_improvement = false;

                                if (oneblock) {
                                    int64_t bbox2[4] = {0};
                                    xlt.shift(bbox[0], bbox[1]).getrect(bbox2);
                                    bbox2[0] -= ideal[0];
                                    bbox2[1] -= ideal[1];
                                    altbdiff = (bbox2[0] * bbox2[0]) + (bbox2[1] * bbox2[1]);
                                    oneblock_improvement = (std::sqrt((double) altbdiff) <= std::sqrt((double) bdiff) - 25.0 + 0.1 * initbail);
                                    trythis = (altbdiff == 0) || oneblock_improvement;
                                } else if (lexrepr == "xs4_33") {
                                    if (trycount == deep_bailout) {
                                        std::cout << "Reached bailout for strategy 'deep'" << std::endl;
                                    } else if (trycount == tree_bailout) {
                                        std::cout << "Reached bailout for strategy 'tree'" << std::endl;
                                    } else if (trycount > tree_bailout) { break; }
                                    altstelllength = stlength(altstell - avoid, cfier);

                                    trythis = (altstelllength <= stelllength - 15.0) || (trycount < deep_bailout) || (nbespoke >= 1);
                                } else {
                                    trythis = true;
                                }

                                if (trythis) {

                                  pattern slt = source;
                                  for (uint64_t m = 0; m < cs.gliders.size(); m++) {
                                      std::pair<int16_t, uint8_t> ng = cs.gliders[m];
                                      int64_t posback = (m + 1) * 128;
                                      slt += eglider(posback + ng.first, posback)[ng.second];
                                  }
                                  uint64_t j2 = (cs.transpose ? 2 : 0) + cs.age;
                                  j2 ^= j;
                                  slt = slt.shift(-cs.dx, -cs.dy);
                                  pattern xlt = source.shift(-cs.dx, -cs.dy);
                                  if (j2 & 1) { slt = slt[1]; xlt = xlt[1]; }
                                  if (j2 & 2) { slt = slt.transpose(); xlt = xlt.transpose(); }
                                  pattern sltshift = slt.shift(bbox[0], bbox[1]);
                                  pattern newpat = sltshift + remainder;

                                  if (newpat[512 * (cs.gliders.size() + 1)] == stell) {
                                    if ((sltshift.convolve(sword) & remainder).empty()) {
                                        // std::cout << "Good match!" << std::endl;
                                    } else {
                                        // std::cout << "Inaccessible from infinity" << std::endl;
                                        continue;
                                    }

                                    int64_t posback = (cs.gliders.size() + 2) * 128;
                                    newpat += exsalvo(posback, posback);
                                    pattern altstell = remainder + xlt.shift(bbox[0], bbox[1]);
                                    if (oneblock) {
                                        if (altbdiff == 0) {
                                            std::cout << "\033[32;1mInitial block correctly emplaced\033[0m" << std::endl;
                                            return newpat;
                                        } else if (oneblock_improvement) {
                                            std::cout << "\033[32;1mInitial block moved towards target\033[0m" << std::endl;
                                            return newpat;
                                        }
                                    } else if (lexrepr == "xs4_33") {
                                        pattern newpat2 = newpat;
                                        if ((trycount >= lastbail) && (trycount < deep_bailout)) {
                                            // We now create a really large sword to ensure only the BR-most
                                            // block is moved by the 'deep' strategy:
                                            pattern qlt = sltshift.convolve(bigblock).convolve(bigblock);
                                            qlt += qlt(8, 8).convolve(bigblock).convolve(bigblock);
                                            if ((qlt.convolve(sword) & remainder).empty()) {
                                                newpat2 = precurse(newpat, cfier, 0, 1, 1, 0, ideal);
                                            }
                                        }

                                        if (newpat != newpat2) {
                                            std::cout << "\033[32;1mSimplification made by strategy 'deep'\033[0m" << std::endl;
                                            return newpat2;
                                        }

                                        double nutil_old = stelllength;
                                        double nutil_new = altstelllength;

                                        if (nbespoke > 0) {

                                            pattern barrier = avoid.convolve(sword);
                                            pattern altrem = altstell - avoid;
                                            pattern rem = stell - avoid;
                                            rem += rem(0, 6); rem += rem(6, 0);
                                            altrem += altrem(0, 6); altrem += altrem(6, 0);
                                            rem += rem(0, 12); rem += rem(12, 0);
                                            altrem += altrem(0, 12); altrem += altrem(12, 0);
                                            uint64_t pop1 = 0;
                                            uint64_t pop2 = 0;
                                            for (uint64_t zz = 0; zz < 4; zz++) {
                                                pop2 += (barrier & altrem).popcount((1 << 30) + 3);
                                                pop1 += (barrier & rem).popcount((1 << 30) + 3);
                                                barrier = barrier.convolve(sword).convolve(sword);
                                            }
                                            nutil_old += 0.1 * pop1;
                                            nutil_new += 0.1 * pop2;
                                        }

                                        if (nutil_new <= nutil_old - 15.0) {
                                            std::cout << "\033[32;1mSimplification made by strategy 'tree'\033[0m: ";
                                            std::cout << "loss " << nutil_old << " --> " << nutil_new << std::endl;
                                            return newpat;
                                        }
                                    } else {
                                        if (is_bespoke) {
                                            std::cout << "\033[32;1mSimplification made by strategy 'bespoke'\033[0m: " ;
                                        } else if (totobj == 1) {
                                            std::cout << "\033[32;1mSimplification made by strategy 'reduce'\033[0m: " ;
                                        } else {
                                            std::cout << "\033[32;1mSimplification made by strategy 'split'\033[0m: ";
                                        }
                                        std::cout << "population " << stell.popcount((1 << 30) + 3) << " --> ";
                                        std::cout << altstell.popcount((1 << 30) + 3) << std::endl;
                                        return newpat;
                                    }
                                  }
                                }

                                trycount += 1;
                            }
                        }
                    }
                  }
                }
            }
            // std::cout << "--------------------------------" << std::endl;
            if ((maxbail != 0) && (initbail < maxbail)) {
                std::cout << "Increasing bailout to " << (initbail * 3) << std::endl;
                return precurse(orig, cfier, state, initbail * 3, maxbail, initbail, ideal);
            }
            return orig;
        }

        pattern preiterate(pattern initial, classifier &cfier, int64_t *ideal, uint64_t depth = 0) {
            pattern pcend = initial;
            pattern pcstart(initial.getlab(), "", initial.getrule());

            pattern gliders(initial.getlab(), "", initial.getrule());

            while (pcstart != pcend) {

                pcstart = pcend & pcend[8];
                pattern salvo = pcend - pcstart;
                if (salvo.nonempty() && gliders.nonempty()) {

                    int64_t diag = 0;
                    int64_t gliders_bbox[4] = {0};
                    int64_t pcend_bbox[4] = {0};

                    gliders.getrect(gliders_bbox);
                    pcend.getrect(pcend_bbox);

                    int64_t horizontal = (pcend_bbox[0] + pcend_bbox[2] - gliders_bbox[0]);
                    int64_t vertical   = (pcend_bbox[1] + pcend_bbox[3] - gliders_bbox[1]);

                    if (diag < horizontal) { diag = horizontal; }
                    if (diag < vertical) { diag = vertical; }

                    diag += 512;
                    diag -= (diag & 255);

                    gliders = gliders(diag, diag);
                }

                gliders += salvo;

                {
                    // save progress
                    std::ostringstream ss;
                    ss << "current" << depth << ".mc";
                    std::ofstream out(ss.str());
                    (pcstart + gliders).write_macrocell(out);
                }

                pattern inpat2 = pcstart;

                if (has_isolated_block(pcstart)) {
                    pattern chunk = get_lowerright(pcstart, inpat2, 8, 32, cfier);
                    if (chunk != pcstart) {
                        pattern remainder = pcstart - chunk;
                        pcend = remainder + preiterate(chunk, cfier, 0, depth+1);
                        continue;
                    }
                    inpat2 -= get_isolated_block(inpat2);
                }

                pcend = precurse(pcstart, cfier, 0, 1, 1, 0, ideal);
                if (pcend != pcstart) { continue; }

                if (inpat2.nonempty()) {
                    pattern chunk = get_lowerright(pcstart, inpat2, 8, 25, cfier);
                    if (chunk != pcstart) {
                        pattern remainder = pcstart - chunk;
                        pcend = remainder + preiterate(chunk, cfier, 0, depth+1);
                        continue;
                    }
                }

                // attempt deeper constructions:
                pcend = precurse(pcstart, cfier, 1, 1, 4096, 0, ideal);
            }

            return (pcstart + gliders);
        }

        pattern preiterate(pattern initial, classifier &cfier) {
            pattern findme(initial.getlab(), "3b2o$2bo2bo$bob2obo$obo2bobo$obo2bobo$bob2obo$2bo2bo$3b2o!", initial.getrule());
            pattern m = initial.match(findme);
            pattern im = initial - m.convolve(findme);

            if (m.empty()) {
                return preiterate(im, cfier, 0);
            } else {
                int64_t ideal[4] = {0};
                m(3, 3).getrect(ideal);
                return preiterate(im, cfier, ideal);
            }
        }

    };

}


