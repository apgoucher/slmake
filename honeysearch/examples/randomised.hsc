# A simple configuration file for HoneySearch.
#
# Adam P. Goucher, 2016-12-23

key #anon

# Consider blocks, tubs, and beehives as seeds:
addseed oo$oo!
addseed bo$obo$bo!
addseed boo$obbo$boo!

iflags 22

# We're uninterested in common objects:
boring xs4_33
boring xp2_7
boring xs6_696
boring xq4_153
boring xs7_2596
boring xs5_253
boring xs6_356
boring xs4_252
boring xs8_6996
boring xs7_25ac
boring xp2_7e
boring xs12_g8o653z11
boring xp2_318c
boring xs6_25a4
boring xs14_g88m952z121
boring xs8_69ic

# Ensure patterns stabilise within 512 generations:
timelimit 512

# Parallelise on 3 cores with 250 MB for each core:
threadcount 3
threadmem 250

# Bombard with three gliders, ensuring new targets fit into 48 x 48:
innersize 48
bombard 3

# Bombard with another glider, ensuring new targets fit into 40 x 40:
innersize 40
bombard 1

# Bombard with another glider, ensuring new targets fit into 32 x 32:
innersize 32
bombard 1

# Forget progress:
forget census
forget alloccur

# Bombard 0.1% of the targets with another glider:
random 1000

# Bombard with three further gliders:
outersize 40
innersize 24
bombard 3

# Bombard 0.1% of the targets with another two gliders:
random 1000
bombard 1

# Bombard with a further glider, producing only interesting targets:
outersize 48
innersize 32
iflags 66584
bombard 1

# Only single objects and exciting targets:
iflags 69632
bombard 1

# Only sparse exciting targets:
iflags 1048576
bombard 1

# No new objects:
iflags 0
bombard 1

upload results
# print cat
