# A simple configuration file for HoneySearch.
#
# Adam P. Goucher, 2017-01-15

# Consider blocks, tubs, and beehives as seeds:
addseed oo$oo!
addseed bo$obo$bo!
addseed boo$obbo$boo!

iflags 22

# We're uninterested in common objects:
boring xs4_33
boring xp2_7
boring xs6_696
boring xq4_153
boring xs7_2596
boring xs5_253
boring xs6_356
boring xs4_252
boring xs8_6996
boring xs7_25ac
boring xp2_7e
boring xs12_g8o653z11
boring xp2_318c
boring xs6_25a4
boring xs14_g88m952z121
boring xs8_69ic

# Eater2 variants are exciting:
exciting xs19_4aar0rbzx121
exciting xs19_3pm0mmzw343
exciting xs19_br0raa4z23
exciting xs20_gbbgra96z11
exciting xs20_gbbgraicz11
exciting xs20_69ar0rbzx121
exciting xs20_br0ra96z23
exciting xs20_br0raicz23
exciting xs20_br0rq23z23
exciting xs21_br0raicz01221
exciting xs21_br0raicz0123
exciting xs21_0br0raa4z643
exciting xs21_3pm0mmz06943
exciting xs21_mm0mp3z6943
exciting xs22_gbbgraarz11
exciting xs26_br0raarz230321
exciting xs27_br0raacz2303453

# Snark precursors are also exciting:
exciting xs22_g89fgkczd553
exciting xs24_0g89fgkczol553

# Ensure patterns stabilise within 512 generations:
timelimit 512

# Parallelise on 64 cores with 100 MB for each core:
threadcount 64
threadmem 100

# Bombard with three gliders, ensuring new targets fit into 48 x 48:
innersize 48
bombard 3

# Bombard with another glider, ensuring new targets fit into 40 x 40:
innersize 40
bombard 1

# Bombard with another glider, ensuring new targets fit into 32 x 32:
innersize 32
bombard 1

# Bombard with three further gliders:
outersize 40
innersize 24
bombard 3

# Bombard with a further glider, producing only interesting targets:
outersize 48
innersize 32
iflags 66584
bombard 1

# Only single objects and exciting targets:
iflags 69632
bombard 1

# Only sparse exciting targets:
iflags 1048576
bombard 1

# No new objects:
iflags 0
bombard 1

# upload results
# print xwss

# Print everything interesting to stdout:
threadcount 71

term 0
diterm 1
traverse 10
assemble 20

diterm 2
ditermfilter xs4_33
traverse 10
print atomic

# Thank you for using HoneySearch; have a wonderful day!
