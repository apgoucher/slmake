# A simple configuration file for HoneySearch.
#
# Adam P. Goucher, 2016-12-23

# Use a block as the initial seed:
addseed oo$oo!

iflags 22

# We're uninterested in common objects:
boring xs4_33
boring xp2_7
boring xs6_696
boring xq4_153
boring xs7_2596
boring xs5_253
boring xs6_356
boring xs4_252
boring xs8_6996
boring xs7_25ac
boring xp2_7e
boring xs12_g8o653z11
boring xp2_318c
boring xs6_25a4
boring xs14_g88m952z121
boring xs8_69ic

# Ensure patterns stabilise within 512 generations:
timelimit 512

# Parallelise on 32 cores with 750 MB for each core:
threadcount 32
threadmem 750

# Bombard with three gliders, ensuring new targets fit into 48 x 48:
innersize 48
bombard 3

# Bombard with another glider, ensuring new targets fit into 40 x 40:
innersize 40
bombard 1

# Bombard with another glider, ensuring new targets fit into 32 x 32:
innersize 32
bombard 1

# Bombard with three further gliders:
outersize 40
innersize 24
bombard 3

# Bombard with a final glider, producing no new targets:
iflags 0
bombard 1

# Print the transitions to stdout:
# print all

print xwss
term 0
diterm -1
traverse 9

# Thank you for using HoneySearch; have a wonderful day!
