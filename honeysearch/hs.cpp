#include "../lifelib/pattern2.h"
#include "../lifelib/classifier.h"
#include "digests/md5.h"
#include "digests/sha256.h"
#include "digests/payosha256.h"

#include <algorithm>
#include <cstdlib>
#include <ctime>
#include <cmath>
#include <unistd.h>
#include <omp.h>
#include <set>

/*
 * Produce a new seed based on the original seed, current time and PID:
 */
std::string reseed(std::string seed) {

    std::ostringstream ss;
    ss << seed << " " << clock() << " " << time(NULL) << " " << getpid();

    std::string prehash = ss.str();

    unsigned char digest[SHA256::DIGEST_SIZE];
    std::memset(digest,0,SHA256::DIGEST_SIZE);

    SHA256 ctx = SHA256();
    ctx.init();
    ctx.update( (unsigned char*)prehash.c_str(), prehash.length());
    ctx.final(digest);

    const char alphabet[] = "abcdefghijkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789";

    std::ostringstream newseed;
    newseed << "h_";
    for (int i = 0; i < 12; i++) {
        newseed << alphabet[digest[i] % 56];
    }

    return newseed.str();

}


typedef std::pair<uint64_t, std::pair<uint8_t, uint8_t> > HoneyMeta;
typedef std::pair<std::string, HoneyMeta> HoneyTarget;

struct Transition {

    uint32_t idx_a;
    uint32_t idx_b;
    int16_t dx;
    int16_t dy;
    char transformation;
    char locality;
    char laneparity;
    int8_t lanenum;

};

struct GliderRecipe {

    uint64_t idx_a;
    uint64_t idx_b;
    std::vector<std::pair<int16_t, char> > gliders;
    int16_t dx;
    int16_t dy;
    bool age;
    uint8_t good;
    bool transpose;
    bool terminal;

    void printseq() {
        if (good == 1) {std::cout << "GOOD : "; }
        if (good == 2) {std::cout << "BAD : "; }
        std::cout << gliders.size() << ": (" << dx << ", " << dy << ")";
        std::cout << (transpose ? 'T' : 'O') << " :";
        for (uint64_t i = 0; i < gliders.size(); i++) {
            std::cout << ' ' << ((int) gliders[i].first) << gliders[i].second;
        }
        if (gliders.size() == 0) { std::cout << " (no gliders)"; }
        if (terminal) {
            std::cout << '*';
        } else {
            std::cout << " : " << (age ? "odd" : "even");
        }
        std::cout << std::endl;
    }

    std::string compress() {
        std::ostringstream ss;
        for (int64_t i = gliders.size()-1; i >= 0; i--) {
            int lane = gliders[i].first;
            int neg = (lane < 0);
            int odd = (gliders[i].second == 'O');
            lane = ((neg ? (-lane) : lane) - 1) >> 1;
            int firstchar = odd + (neg << 1) + ((lane & 7) << 2);
            ss << "0123456789ABCDEFGHIJKLMNOPQRSTUV"[firstchar];
            lane = lane >> 3;
            while (lane > 0) {
                ss << "abcdefghijklmnop"[lane & 15];
                lane = lane >> 4;
            }
        }
        std::string x = ss.str();
        std::reverse(x.begin(), x.end());
        return x;
    }

    uint64_t length() {
        return ((gliders.size() == 0) ? 0 : (gliders.size() - terminal));
    }

};

// Category-theoretic composition of glider recipes:
GliderRecipe operator+(const GliderRecipe &lhs, const GliderRecipe &rhs) {

    GliderRecipe newrec = lhs;
    if (lhs.terminal) { newrec.gliders.pop_back(); }
    for (uint64_t i = 0; i < rhs.gliders.size(); i++) {
        int16_t lane = rhs.gliders[i].first;
        char parity = rhs.gliders[i].second;
        if (lhs.age) { parity = 148 - parity; }
        if (lhs.transpose) { lane = -lane; }
        lane += 2 * (lhs.dx - lhs.dy);
        newrec.gliders.push_back(std::pair<int16_t, char>(lane, parity));
    }
    newrec.idx_a = lhs.idx_a;
    newrec.idx_b = rhs.idx_b;
    newrec.dx += (lhs.transpose ? rhs.dy : rhs.dx);
    newrec.dy += (lhs.transpose ? rhs.dx : rhs.dy);
    newrec.age ^= rhs.age;
    newrec.transpose ^= rhs.transpose;
    newrec.terminal = rhs.terminal;
    return newrec;

}


class HoneyThread {

    public:

    std::string rulestring;
    apg::lifetree<uint32_t, 1> lt;
    apg::pattern eglider;
    apg::pattern oglider;
    apg::pattern diagonal;

    apg::classifier cfier;
    std::map<std::string, int64_t> census;
    std::map<std::string, std::vector<std::string> > alloccur;

    std::map<uint64_t, uint64_t> dig2ind;
    std::vector<HoneyTarget> targets;
    std::vector<GliderRecipe> minimals;
    std::vector<uint64_t> local2global;
    std::vector<Transition> translist;

    std::vector<std::string> xwssgen;

    std::map<uint64_t, std::map<uint64_t, std::vector<GliderRecipe> > > atomics;

    HoneyThread(uint32_t maxmem, std::string rule) : rulestring(rule),
                                   lt(maxmem),
                                   eglider(&lt, "3o$o$bo!", rule),
                                   oglider(&lt, "boo$oo$bbo!", rule),
                                   diagonal(&lt, "o$bo$2bo$3bo$4bo$5bo$6bo$7bo$8bo$9bo$10bo$11bo$12bo$13bo$14bo$15bo!", rule),
                                   cfier(&lt, rule)
    {
        diagonal += diagonal(16, 16);
        diagonal += diagonal(32, 32);
        diagonal += diagonal(-64, -64);
    }

    std::vector<std::pair<int64_t, std::string> > getSortedList(int64_t &totobjs) {

        std::vector<std::pair<int64_t, std::string> > censusList;
        std::map<std::string, int64_t>::iterator it;
        for (it = census.begin(); it != census.end(); it++)
        {
            if (it->second != 0) {
                censusList.push_back(std::pair<int64_t, std::string>(it->second, it->first));
                totobjs += it->second;
            }
        }
        std::sort(censusList.begin(), censusList.end());
        return censusList;

    }

    std::string getResults(std::string root) {

        int64_t totobjs = 0;

        std::vector<std::pair<int64_t, std::string> > censusList = getSortedList(totobjs);

        std::ostringstream ss;

        ss << "@VERSION hs1.0\n";
        ss << "@MD5 " << md5(root) << "\n";
        ss << "@ROOT " << root << "\n";
        ss << "@RULE " << rulestring << "\n";
        ss << "@SYMMETRY SS\n";
        ss << "@NUM_SOUPS " << targets.size() << "\n";
        ss << "@NUM_OBJECTS " << totobjs << "\n";

        ss << "\n@CENSUS TABLE\n";

        for (int i = censusList.size() - 1; i >= 0; i--) {
            int64_t occount = censusList[i].first;
            int64_t prcount = alloccur[censusList[i].second].size();
            if ((prcount > 0) && (prcount < 15)) { occount = prcount; }
            ss << censusList[i].second << " " << occount << "\n";
        }

        ss << "\n@SAMPLE_SOUPIDS\n";
        for (int i = censusList.size() - 1; i >= 0; i--) {
            std::vector<std::string> occurrences = alloccur[censusList[i].second];
            ss << censusList[i].second;
            for (uint64_t j = 0; j < occurrences.size(); j++) {
                ss << " " << occurrences[j];
            }
            ss << "\n";
        }

        return ss.str();
    }

    std::string submitResults(std::string payoshakey, std::string root) {

        std::string authstring = authenticate(payoshakey.c_str(), "post_apgsearch_haul");

        // Authentication failed:
        if (authstring.length() == 0)
            return "";

        std::string newres = authstring + "\n" + getResults(root);
        return catagolueRequest(newres.c_str(), "/apgsearch");

    }


    std::string targrle(HoneyTarget ht) {

        std::string tstring = ht.first;
        apg::pattern target(&lt, lt._string32(tstring), rulestring);
        apg::bitworld bw = target.shift(16, 16).flatlayer(0);
        int64_t bbox[4] = {0};
        bw.getbbox(bbox);
        int64_t maxx = bbox[0] + bbox[2];
        int64_t maxy = bbox[1] + bbox[3];
        std::ostringstream ss;

        for (int64_t y = 0; y < maxy; y++) {
            uint64_t c = 1;
            for (int64_t x = 0; x < maxx; x++) {
                if (bw.getcell(x, y) != bw.getcell(x+1, y)) {
                    char ctype = (bw.getcell(x, y) ? 'o' : 'b');
                    if (c > 2) { ss << c; }
                    if (c == 2) { ss << ctype; }
                    ss << ctype;
                    c = 1;
                } else {
                    c += 1;
                }
            }
            ss << ((y + 1 == maxy) ? '!' : '$');
        }

        return ss.str();
    }

    void setrule(std::string rule) {
        cfier = apg::classifier(&lt, rule);
        eglider.setrule(rule);
        oglider.setrule(rule);
        diagonal.setrule(rule);
        rulestring = rule;
    }

    std::pair<std::string, HoneyMeta> canonise(apg::pattern s0, apg::pattern s1) {

        apg::pattern t0 = s0.transform("transpose", 0, 0);
        apg::pattern t1 = s1.transform("transpose", 0, 0);

        uint8_t newflags = 1;
        uint64_t candig = s0.digest();
        apg::pattern canpat = s0;
        uint8_t canphase = 0;

        if (s0 == s1) {
            if (s0 == t0) {
                newflags = 15;
            } else {
                newflags = 3;
                uint64_t newdig = t0.digest();
                if (newdig < candig) { candig = newdig; canphase = 2; canpat = t0; }
            }
        } else {
            uint64_t newdig = s1.digest();
            if (newdig < candig) { candig = newdig; canphase = 1; canpat = s1; }
            if (s0 == t0) {
                newflags = 5;
            } else if (s0 == t1) {
                newflags = 9;
            } else {
                newdig = t0.digest();
                if (newdig < candig) { candig = newdig; canphase = 2; canpat = t0; }
                newdig = t1.digest();
                if (newdig < candig) { candig = newdig; canphase = 3; canpat = t1; }
            }
        }

        std::string s32 = canpat._string32();

        // Make the digest more collision-resistant:
        uint64_t j = 0;
        for (uint64_t i = 0; i < s32.size(); i++) {
            j += (uint64_t) s32[i];
            j *= (1 + (i << 4));
        }
        candig += j;

        HoneyMeta hm(candig, std::pair<uint8_t, uint8_t>(newflags, canphase));
        return std::pair<std::string, HoneyMeta>(s32, hm);
    }

    void printtrans(Transition tn) {
        HoneyTarget ta = targets[tn.idx_a];
        HoneyTarget tb = targets[tn.idx_b];
        std::cout << "nst.f..b"[(ta.second.second.first >> 1) & 7] << "[" << tn.idx_a << "]";
        std::cout << (((int) tn.lanenum) * 2 + 1) << tn.laneparity;
        std::cout << ":(" << tn.dx << "," << tn.dy << ")" << tn.transformation;
        std::cout << "nst.f..b"[(tb.second.second.first >> 1) & 7] << "[" << tn.idx_b << "]";
        std::cout << tn.locality << std::endl;
    }

    void globalise(HoneyThread *glob, std::vector<std::vector<Transition> > &tlib) {

        uint64_t ll = local2global.size();

        while (ll < targets.size()) {
            HoneyTarget ht = targets[ll];
            uint64_t digest = ht.second.first;
            uint64_t idx = 0xffffffffffffffffull;
            if (glob->dig2ind.find(digest) == glob->dig2ind.end()) {
                idx = glob->targets.size();
                glob->targets.push_back(ht);
                GliderRecipe gr = minimals[ll];
                gr.idx_b = idx;
                glob->minimals.push_back(gr);
                glob->dig2ind[digest] = idx;
            } else {
                uint64_t idy = glob->dig2ind[digest];
                std::string sa = ht.first;
                std::string sb = glob->targets[idy].first;
                if (sa.substr(0, sa.find(' ')) == sb.substr(0, sb.find(' '))) {
                    idx = idy;
                } else {
                    std::cerr << "Hash collision during globalisation!" << std::endl;
                    idx = -1;
                }
            }
            local2global.push_back(idx);
            ll += 1;
        }

        while (translist.size() > 0) {
            Transition tn = translist.back();
            if (tn.locality == 'l') {
                uint64_t idx = local2global[tn.idx_b];
                tn.locality = (idx == 0xffffffffffffffffull) ? '0' : 'g';
                tn.idx_b = idx;
            }
            translist.pop_back();
            // Append to the transition library:
            if (tn.locality != '0') {
                if (tlib.size() <= tn.idx_a) {tlib.resize(tn.idx_a + 1); }
                tlib[tn.idx_a].push_back(tn);
            } else {
                std::cerr << "Omitting transition due to invalid target." << std::endl;
            }
        }

        while (xwssgen.size() > 0) {
            std::string s = xwssgen.back();
            xwssgen.pop_back();
            glob->xwssgen.push_back(s);
        }

        for (auto it = census.begin(); it != census.end(); ++it) {
            glob->census[it->first] += (it->second);
        }

        for (auto it = alloccur.begin(); it != alloccur.end(); ++it) {
            for (uint64_t i = 0; i < it->second.size(); i++) {
                if (glob->alloccur[it->first].size() < 20) {
                    bool prefix = false;
                    for (uint64_t j = 0; j < glob->alloccur[it->first].size(); j++) {
                        std::string s = glob->alloccur[it->first][j];
                        if (s.size() <= it->second[i].size()) {
                            if (it->second[i].substr(0, s.size()) == s) { prefix = true; }
                        }
                    }
                    if (prefix == false) { glob->alloccur[it->first].push_back(it->second[i]); }
                } else { break; }
            }
        }
    }

    void evaluate(apg::pattern seed, int elapsed, int i, uint8_t j, HoneyThread *glob,
                  int innersize, int outersize, uint64_t iflags, std::map<std::string, int32_t> *borings) {

        int64_t bbox[4];
        apg::pattern seed1 = seed[1];
        (seed + seed1).getrect(bbox);
        int64_t left = bbox[0]; int64_t top = bbox[1];
        int64_t bbsize = (bbox[2] < bbox[3]) ? bbox[3] : bbox[2];

        // Produce a canonical form:
        apg::pattern s0 = seed.shift(-16-left, -16-top);
        apg::pattern s1 = seed1.shift(-16-left, -16-top);
        std::pair<std::string, HoneyMeta> x = canonise(s0, s1);

        char locality = '0';
        uint64_t idx = 0;
        uint64_t candig = x.second.first;

        std::map<uint64_t, uint64_t> *mmap = 0;
        if (glob != 0) { mmap = &(glob->dig2ind); }
        if ((mmap != 0) && (mmap->find(candig) != mmap->end())) {
            // Cached globally
            idx = (*mmap)[candig];
            std::string s32 = x.first;
            if (s32 == glob->targets[idx].first.substr(0, s32.size())) {
                locality = 'g';
            } else {
                std::cerr << "Global hash collision!" << std::endl;
            }
        } else if (dig2ind.find(candig) != dig2ind.end()) {
            // Cached locally
            idx = dig2ind[candig];
            std::string s32 = x.first;
            if (s32 == targets[idx].first.substr(0, s32.size())) {
                locality = 'l';
            } else {
                std::cerr << "Local hash collision!" << std::endl;
            }
        } else {
            // Discard the result if too large and messy:
            if ((outersize > 0) && (bbsize > outersize)) { return; }

            // Uncached
            apg::bitworld live = seed.flatlayer(0);
            apg::bitworld env = (seed + seed1).flatlayer(0);
            std::map<std::string, int64_t> counts = cfier.census(live, env);

            std::ostringstream ss;
            // ss << x.first._string32();
            ss << x.first;
            std::ostringstream ss2;
            ss2 << elapsed << " " << 2*i+1 << "EO"[j];

            bool small = (bbsize <= innersize);
            int rareobj = 0;
            uint64_t totobj = 0;

            for (auto it = counts.begin(); it != counts.end(); ++it) {
                ss << " " << it->first << "(" << it->second << ")";
                ss2 << " " << it->first << "(" << it->second << ")";
                if ((borings != 0) && ((*borings)[it->first] == 0)) { rareobj |= 1; }
                if ((borings != 0) && ((*borings)[it->first] == -1)) { rareobj |= 2; }
                totobj += it->second;
            }

            std::set<std::string> rareoccur;
            for (auto it = counts.begin(); it != counts.end(); ++it) {
                census[it->first] += (it->second);
                if (alloccur[it->first].size() < 3) { rareoccur.insert(it->first); }
                if (alloccur[it->first].size() + (totobj * 2) < 10) { rareoccur.insert(it->first); }
            }

            if (rareobj >= 2) {
                xwssgen.push_back(ss2.str());
                std::cerr << "Exciting object in ash: " << ss2.str() << std::endl;
            }

            if (outersize == 0) { totobj = innersize; }

            ss << " {" << totobj << "}";

            bool twoobj   = (totobj == 1) || (totobj == 2);
            bool threeobj = (totobj == 1) || (totobj == 3);

            uint64_t xsum = (uint64_t) small;
            xsum |= (((uint64_t) (rareobj >= 1)) << 1);
            xsum |= (((uint64_t) twoobj) << 2);
            xsum |= (((uint64_t) threeobj) << 3);
            xsum |= (((uint64_t) (rareobj >= 2)) << 4);

            bool interesting = ((apg::upset(iflags) >> xsum) & 1);

            char transformation = "NATF"[x.second.second.second];

            GliderRecipe latestmod;
            latestmod.idx_a = elapsed;
            latestmod.idx_b = 0;
            latestmod.dx = 0;
            latestmod.dy = 0;
            latestmod.good = 0;
            latestmod.transpose = 0;
            latestmod.age = 0;
            latestmod.terminal = false;

            if (glob != 0) {
                latestmod.transpose = (transformation % 13) ? 1 : 0;
                latestmod.age = (transformation % 3) ? 1 : 0;
                if ((x.second.second.first & 15) == 9) { latestmod.age ^= latestmod.transpose; }
                latestmod.transpose = (x.second.second.first & 12) ? 0 : latestmod.transpose;
                latestmod.dx = left + 16;
                latestmod.dy = top + 16;
                latestmod.gliders.push_back(std::pair<int16_t, char>(i*2+1, "EO"[j]));
                latestmod = glob->minimals[elapsed] + latestmod;
            } else {
                latestmod.idx_a = targets.size();
            }

            if ((glob == 0) || interesting) {
                locality = 'l'; idx = targets.size();
                latestmod.idx_b = idx;
                dig2ind.emplace(candig, idx);
                targets.push_back(HoneyTarget(ss.str(), x.second));
                minimals.push_back(latestmod);
            }

            if ((glob != 0) && (rareoccur.size() != 0)) {
                std::string cmpr = "_" + targrle(glob->targets[latestmod.idx_a]) + latestmod.compress();
                for (auto it = rareoccur.begin(); it != rareoccur.end(); ++it) {
                    alloccur[(*it)].push_back(cmpr);
                }
                // std::cerr << cmpr << std::endl;
            }
        }

        // Commit transition to log:
        if ((locality != '0') && (outersize != 0)) {
            Transition tn;
            tn.idx_a = elapsed;
            tn.idx_b = idx;
            tn.dx = left + 16;
            tn.dy = top + 16;
            tn.transformation = "NATF"[x.second.second.second];
            tn.locality = locality;
            tn.laneparity = "EO"[j];
            tn.lanenum = i;
            translist.push_back(tn);
        }
    }

    void addseed(std::string rle) {

        apg::pattern seed(&lt, rle, rulestring);
        evaluate(seed, 0, 0, 0, 0, 0, 0, 0, 0);

    }

    void blockseed(std::string rle, int innersize) {
        apg::pattern rawseed(&lt, rle, rulestring);
        if (rawseed != rawseed[2]) { return; }
        evaluate(rawseed, 0, 0, 0, 0, 0, 0, 0, 0);
        apg::pattern block(&lt, "oo$oo!", rulestring);
        apg::pattern twenty1(&lt, "b3o$5o$5o$5o$b3o!", rulestring);
        apg::pattern bigrect(&lt, lt.rectangle(innersize, innersize), rulestring);
        twenty1 = twenty1.convolve(bigrect).shift(1-innersize, 1-innersize);
        apg::pattern donottouch = (rawseed + rawseed[1]).convolve(twenty1).shift(-2, -2);
        apg::pattern smallrect(&lt, lt.rectangle(8, 8), rulestring);
        apg::pattern musttouch = donottouch.convolve(smallrect);
        for (int i = 0; i < innersize-1; i++) {
            for (int j = 0; j < innersize-1; j++) {
                apg::pattern transblock = block(i, j);
                if ((transblock & donottouch).gethnode().index == 0) {
                    if ((transblock & musttouch).gethnode().index != 0) {
                        apg::pattern seed = transblock + rawseed;
                        if (seed != seed[2]) {
                            std::cerr << "Impossible!" << std::endl;
                        } else {
                            evaluate(seed, 0, 0, 0, 0, 1, 0, 0, 0);
                        }
                    }
                }
            }
        }
    }

    void verify(GliderRecipe &gr, HoneyThread *glob, int timelimit) {

        std::string tstring_a = glob->targets[gr.idx_a].first;
        apg::pattern target_a(&lt, lt._string32(tstring_a), rulestring);
        std::string tstring_b = glob->targets[gr.idx_b].first;
        apg::pattern target_b(&lt, lt._string32(tstring_b), rulestring);

        apg::pattern envelope = target_a + target_a[1];
        int64_t bbox[4] = {0};
        envelope.getrect(bbox);
        int emaxx = bbox[0] + bbox[2] + 4;
        int emaxy = bbox[1] + bbox[3] + 4;
        int safespace = (emaxx > emaxy) ? emaxx : emaxy;

        int length = gr.length();
        for (int k = 0; k < length; k++) {
            std::pair<int16_t, char> g = gr.gliders[k];
            int i = (g.first - 1) / 2;
            int tx = (i < 0) ? safespace + i : safespace;
            int ty = (i > 0) ? safespace - i : safespace;
            if (g.second == 'E') { target_a += eglider(tx, ty); }
            if (g.second == 'O') { target_a += oglider(tx, ty); }
            safespace += (timelimit / 4);
        }

        apg::pattern result = target_a[timelimit * length];
        if (gr.transpose) { target_b = target_b.transform("transpose", 0, 0); }
        if (gr.age) { target_b = target_b[1]; }
        target_b = target_b(gr.dx, gr.dy);
        gr.good = (result == target_b) ? 1 : 2;
    }

    void collidewith(HoneyTarget ht, uint64_t elapsed, HoneyThread *glob,
                     int innersize, int outersize, uint64_t iflags, std::map<std::string, int32_t> *borings,
                     int timelimit) {

        std::string tstring = ht.first;
        uint8_t flags = ht.second.second.first;
        uint8_t period = (flags & 2) ? 1 : 2;

        apg::pattern target(&lt, lt._string32(tstring), rulestring);
        apg::pattern envelope = target + target[1];
        int64_t bbox[4] = {0};
        envelope.getrect(bbox);

        // We want the glider to be positioned either below, or to the right,
        // of the bounding box of the target envelope. Subject to this, we
        // choose the glider to be as close as possible to the target to
        // reduce the amount of computation we need to perform.
        int emaxx = bbox[0] + bbox[2] + 4;
        int emaxy = bbox[1] + bbox[3] + 4;
        int safespace = (emaxx > emaxy) ? emaxx : emaxy;

        // There is no point testing an input glider if we know that it
        // cannot collide with the target. This allows us to calculate the
        // leftmost and rightmost lane capable of hitting the target:
        envelope.convolve(diagonal).subrect(-64, 0, 128, 1).getrect(bbox);
        int starti = (flags & 12) ? 0 : bbox[0] - 5;
        int endi = bbox[0] + bbox[2] + 4;

        // Iterate over different glider phases:
        for (uint8_t j = 0; j < period; j++) {
            apg::pattern perturber = (j == 0) ? eglider : oglider;
            // Iterate over different glider lanes:
            for (int i = starti; i < endi; i++) {
                int tx = (i < 0) ? safespace + i : safespace;
                int ty = (i > 0) ? safespace - i : safespace;
                apg::pattern seed = target + perturber.shift(tx, ty);
                seed = seed[timelimit];
                apg::pattern seed120 = seed[120];
                int64_t bbox_a[4] = {0}; int64_t bbox_b[4] = {0};
                seed.getrect(bbox_a); seed120.getrect(bbox_b);
                if (seed120.gethnode().index == 0) { continue; }
                if ((bbox_a[2] != bbox_b[2]) || (bbox_a[3] != bbox_b[3])) { continue; }
                if (seed != seed120.shift(bbox_a[0] - bbox_b[0], bbox_a[1] - bbox_b[1])) { continue; }
                /*
                * At this point, we know that the pattern is non-empty and
                * has a period dividing 120.
                */
                if ((bbox_a[0] != bbox_b[0]) || (bbox_a[1] != bbox_b[1])) {
                    // Pattern moves periodically (e.g. one or more gliders):
                    if ((bbox_a[0] == bbox_b[0]) || (bbox_a[1] == bbox_b[1])) {
                        // Orthogonal spaceship:
                        std::ostringstream ss;
                        ss << elapsed << " " << 2*i+1 << "EO"[j];
                        std::map<std::string, int64_t> counts = cfier.census(seed, 120);
                        for (auto it = counts.begin(); it != counts.end(); ++it) {
                            ss << " " << it->first << "(" << it->second << ")";
                        }
                        if (bbox_a[0] + bbox_a[1] < bbox_b[0] + bbox_b[1]) {
                            ss << " BACKWARD";
                        } else {
                            ss << " FORWARD";
                        }
                        xwssgen.push_back(ss.str());
                        std::cerr << "Clean orthogonal spaceship: " << ss.str() << std::endl;
                    }
                } else if (seed == seed[2]) {
                    // Pattern is non-empty and 2-periodic:
                    evaluate(seed, elapsed, i, j, glob, innersize, outersize, iflags, borings);
                }
            }
        }
    }

    void summarise(uint64_t i, std::string desc) {

        std::cout << "-------- " << desc << " [" << i << "] :";
        std::string tstring = targets[i].first;
        std::cout << tstring.substr(tstring.find(' ')) << " --------" << std::endl;
        apg::pattern target(&lt, lt._string32(tstring), rulestring);

        int64_t bbox[4];
        (target + target[1]).getrect(bbox);
        target.display(bbox);

    }
};

/*
* We want to store short byte-sequences of the form:
* [tx][ty][depth|k][[source]][sx][sy][lane]
* to represent each conversion.
*/

typedef std::pair<uint64_t, std::pair<uint8_t, uint8_t> > TransTarget;
typedef std::map<TransTarget, std::vector<uint8_t> > SeqStore;
typedef std::vector<std::vector<uint8_t> > SeqVector;


class Traversal {

    public:

    SeqVector globstore;
    HoneyThread *glob;
    std::vector<std::vector<Transition> > *tlib;

    void addstart(uint64_t tid) {

        std::vector<uint8_t> q;
        q.push_back(128); q.push_back(128); q.push_back(0);
        uint64_t temps = tid << 2;
        q.push_back(temps & 255); temps = temps >> 8;
        while (temps) {
            q.push_back(temps & 255); temps = temps >> 8;
        }
        q[2] |= (q.size() - 4);
        q.resize(q.size() + 3);
        globstore[tid << 1].insert(globstore[tid << 1].end(), q.begin(), q.end());

    }

    int findrecipe(uint64_t s, uint64_t depth, int x, int y, GliderRecipe *gr, std::vector<uint64_t> *intermeds) {

        uint64_t imax = globstore[s].size();
        if (depth == 0) {
            if (gr != 0) { gr->idx_a = (s >> 1); }
            if (intermeds != 0) { intermeds->push_back(s >> 1); }
            return 0;
        }
        uint64_t i = 0;
        while (i < imax) {
            int sx = globstore[s][i++];
            int sy = globstore[s][i++];
            uint64_t p = globstore[s][i++];
            uint64_t k = (p & 7) + 1;
            uint64_t d = p >> 3;
            if ((sx == x) && (sy == y) && (depth == d)) {
                uint64_t t = 0;
                for (uint64_t j = 0; j < k; j++) {
                    t |= (globstore[s][i+j] << (8*j));
                }
                uint8_t age = t & 1;
                t = t >> 1;
                int tx = globstore[s][i+k];
                int ty = globstore[s][i+k+1];
                uint8_t rellane = globstore[s][i+k+2];
                int tz = (rellane >> 1);
                int abslane = (tz + tx - ty) * 2 - 127;
                int lastage = findrecipe(t, depth-1, tx, ty, gr, intermeds);
                if (intermeds != 0) { intermeds->push_back(s >> 1); }
                char lpchar = "EO"[(lastage + rellane) % 2];
                if (gr == 0) {
                    std::cout << " " << abslane << lpchar;
                } else {
                    gr->gliders.push_back(std::pair<int16_t, char>(abslane, lpchar));
                }
                age = (age + lastage) % 2;
                return age;
            }
            i += (k + 3);
        }
        std::cerr << "Abject failure!" << std::endl;
        return -1;

    }

    GliderRecipe make_recipe(uint64_t s, uint64_t depth, int x, int y, std::vector<uint64_t> *intermeds) {
        GliderRecipe gr;
        gr.terminal = false;
        gr.idx_b = s >> 1;
        gr.transpose = (s & 1);
        gr.age = findrecipe(s, depth, x, y, &gr, intermeds);
        gr.dx = x - 128;
        gr.dy = y - 128;
        gr.good = 0;
        return gr;
    }

    GliderRecipe make_recipe(uint64_t s, uint64_t depth, int x, int y, std::string lastglider, std::vector<uint64_t> *intermeds) {
        GliderRecipe gr = make_recipe(s, depth, x, y, intermeds);
        if (lastglider != "") {
            gr.terminal = true;
            int lane = std::stoll(lastglider.substr(0, lastglider.size()-1));
            if (gr.transpose) { lane = 0 - lane; }
            lane += (2 * (x - y));
            char newpar = lastglider[lastglider.size()-1];
            if (gr.age) { newpar = 148 - newpar; }
            gr.gliders.push_back(std::pair<int16_t, char>(lane, newpar));
        }
        return gr;
    }

    void store_trav(uint64_t s, std::string lastglider, HoneyThread *loc, int timelimit) {

        uint64_t imax = globstore[s].size();
        if (imax == 0) { return; }
        uint64_t i = 0;
        while (i < imax) {
            int sx = globstore[s][i++];
            int sy = globstore[s][i++];
            uint64_t p = globstore[s][i++];
            uint64_t k = (p & 7) + 1;
            uint64_t d = p >> 3;
            i += (k + 3);
            std::vector<uint64_t> intermeds;
            GliderRecipe gr = make_recipe(s, d, sx, sy, lastglider, &intermeds);
            auto submapit = glob->atomics.find(gr.idx_a);
            if (submapit != glob->atomics.end()) {
                auto submapit2 = submapit->second.find(gr.idx_b);
                if (submapit2 != submapit->second.end()) {
                    if (loc != 0) { loc->verify(gr, glob, timelimit); }
                    if (gr.good == 2) {
                        std::cerr << "Bad recipe:";
                        for (unsigned int j = 0; j < intermeds.size(); j++) {
                            std::cerr << " " << intermeds[j];
                        }
                        std::cerr << " (!!!)" << std::endl;
                    }
                    submapit2->second.push_back(gr);
                } else {
                    std::cerr << "Error: vector does not exist but should." << std::endl;
                }
            } else {
                std::cerr << "Error: map does not exist but should." << std::endl;
            }
        }
    }

    void iterate(uint64_t depth, uint64_t start, uint64_t finish, uint64_t proccount,
                 std::set<uint64_t> *terminals, std::map<uint64_t, std::string> *diterms) {

        // Define a complete matrix of shape (proccount * proccount)
        // to enable any thread to communicate with any other thread:
        std::vector<std::vector<SeqStore> > locstores;
        for (uint64_t i = 0; i < proccount; i++) {
            locstores.emplace_back(proccount);
        }

        #pragma omp parallel num_threads(proccount)
        {
            uint64_t pc = 0;
            uint64_t locstart = 0;
            #pragma omp critical
            {
                pc = omp_get_thread_num();
                locstart = start - (start % proccount) + pc;
                while (locstart < start) { locstart += proccount; }
            }

            // Compute all collisions with transtargets of depth-1:
            for (uint64_t s = locstart; s < finish; s += proccount) {

                uint64_t imax = globstore[s].size();
                if (imax == 0) { continue; }
                if ((depth > 1) && (terminals != 0) && terminals->count(s >> 1)) { continue; }

                std::vector<uint8_t> q;
                q.push_back(depth << 3);
                uint64_t temps = s << 1;
                q.push_back(temps & 255); temps = temps >> 8;
                while (temps) {
                    q.push_back(temps & 255); temps = temps >> 8;
                }
                q[0] |= (q.size() - 2);
                // q.resize(q.size() + 3);
                q.push_back(0); q.push_back(0); q.push_back(0);

                // Ascertain information about source object:
                HoneyTarget ta = glob->targets[s >> 1];
                uint8_t symstart = s & 1;
                uint8_t symmetries = (ta.second.second.first & 12) ? 2 : 1;

                uint64_t i = 0;
                while (i < imax) {
                    int sx = globstore[s][i++];
                    int sy = globstore[s][i++];
                    q[q.size() - 3] = sx;
                    q[q.size() - 2] = sy;
                    uint64_t p = globstore[s][i++];
                    uint64_t k = (p & 7) + 1;
                    uint64_t d = p >> 3;
                    i += (k + 3);
                    if (d + 1 == depth) {
                        for (uint8_t sym = symstart; sym < symstart + symmetries; sym++) {
                            for (auto it = (*tlib)[s >> 1].begin(); it != (*tlib)[s >> 1].end(); ++it) {
                                Transition tn = *it;
                                // If we're on the final pass, only include interesting leaves;
                                // this saves us oodles of memory:
                                if ((diterms == 0) || (diterms->find(tn.idx_b) != diterms->end())) {
                                    int tx = sx + (sym ? tn.dy : tn.dx);
                                    int ty = sy + (sym ? tn.dx : tn.dy);
                                    HoneyTarget tb = glob->targets[tn.idx_b];
                                    uint8_t orient = (tn.transformation % 13) ? (1-sym) : sym;
                                    uint8_t age = (tn.transformation % 3) ? 1 : 0;
                                    if ((tb.second.second.first & 15) == 9) { age ^= orient; }
                                    orient = (tb.second.second.first & 12) ? 0 : orient;
                                    int tz = sym ? (63 - tn.lanenum) : (64 + tn.lanenum);
                                    bool lparity = (tn.laneparity == 'O');
                                    if (sym && ((ta.second.second.first & 15) == 9)) { lparity = !(lparity); age = 1 - age; }
                                    q[1] &= 254; q[1] |= age;
                                    uint64_t y = (((uint64_t) tn.idx_b) << 1) + orient;
                                    // Ensure target is within 127 cells (L_infinity distance)
                                    // of the initial target:
                                    if ((tx > 0) && (tx <= 255) && (ty > 0) && (ty <= 255) && (tz >= 0) && (tz <= 127)) {
                                        q[q.size() - 1] = (tz << 1) + lparity;
                                        TransTarget tt(y, std::pair<uint8_t, uint8_t>(tx, ty));
                                        locstores[pc][y % proccount].emplace(tt, q);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            #pragma omp barrier

            SeqStore ss;
            std::set<uint64_t> relevants;

            // Move the contents of the processor's locstores into ss,
            // keeping track of which targets are relevant:
            for (uint64_t x = 0; x < proccount; x++) {
                for (auto it = locstores[x][pc].begin(); it != locstores[x][pc].end(); ++it) {
                    ss.emplace(it->first, it->second);
                    relevants.insert(it->first.first);
                }
            }

            // Eliminate redundant (already constructed) transtargets:
            for (auto it = relevants.begin(); it != relevants.end(); ++it) {
                uint64_t targid = *it;
                uint64_t i = 0;
                while (i < globstore[targid].size()) {
                    uint8_t x = globstore[targid][i++];
                    uint8_t y = globstore[targid][i++];
                    uint64_t k = (globstore[targid][i++] & 7) + 1;
                    i += (k + 3);
                    ss.erase(TransTarget(targid, std::pair<uint8_t, uint8_t>(x, y)));
                }
            }

            // Enter new transtargets into the global seqstore:
            for (auto it = ss.begin(); it != ss.end(); ++it) {
                uint64_t targid = it->first.first;
                globstore[targid].push_back(it->first.second.first);
                globstore[targid].push_back(it->first.second.second);
                // Potentially faster than repeated push_back:
                globstore[targid].insert(globstore[targid].end(), it->second.begin(), it->second.end());
            }
        }
    }

    void iterate_full(uint64_t depth, uint64_t proccount, std::set<uint64_t> *terminals,
                      bool progress, std::map<uint64_t, std::string> *diterms) {

        uint64_t elapsed = 0;
        if (tlib->size() < globstore.size()) {
            std::cerr << "  -- resizing..." << std::endl;
            tlib->resize(globstore.size());
        }
        while (elapsed < globstore.size()) {
            uint64_t upto = elapsed + 100000;
            upto = (upto < globstore.size()) ? upto : globstore.size();
            iterate(depth, elapsed, upto, proccount, terminals, diterms);
            elapsed = upto;
            if (progress) { std::cerr << "   -- " << (elapsed/2) << " targets examined." << std::endl; }
        }
        std::cerr << "  -- iterated up to depth " << depth << "." << std::endl;
    }


    Traversal(HoneyThread *glob, std::vector<std::vector<Transition> > *tlib) {
        this->glob = glob;
        this->tlib = tlib;
        globstore.resize(glob->targets.size() * 2);
    }

};

typedef std::pair<std::pair<int16_t, int16_t>, bool> coord16_t;

class HoneySearch {

    std::string rulestring;
    std::string localseed;
    std::string payoshakey;

    uint32_t threadmem;
    uint32_t threadcount;
    uint64_t iflags;
    int innersize;
    int outersize;
    int timelimit;

    HoneyThread glob;
    std::vector<std::vector<Transition> > tlib;
    std::map<std::string, int32_t> borings;
    uint64_t elapsed;
    uint64_t printed;
    uint64_t glex;

    int term_thresh;
    int diterm_thresh;
    std::string term_filter;
    std::string diterm_filter;

    bool safety;

    public:

    void setrule(std::string rule) {
        rulestring = rule;
        glob.setrule(rule);
    }

    HoneySearch(uint32_t maxmem, uint32_t tcount, std::string rule) : glob(maxmem, rule) {
        localseed = reseed("cat");
        rulestring = rule;
        payoshakey = "#anon";
        threadmem = maxmem;
        threadcount = tcount;
        iflags = 22;
        innersize = 32;
        outersize = 48;
        timelimit = 512;
        elapsed = 0;
        printed = 0;
        glex = 0;
        term_thresh = 0;
        diterm_thresh = 1;
        term_filter = "none";
        diterm_filter = "none";
        safety = 0;
    }

    // void hsearch(std::istream &instream);

    void hsearch_file(std::string filename) {
        if (filename == "stdin") {
            hsearch(std::cin);
        } else {
            std::ifstream f(filename);
            hsearch(f);
        }
    }

    void make_molecules(uint64_t maxdepth) {

        uint64_t tcount = 0;
        for (auto it = glob.atomics.begin(); it != glob.atomics.end(); ++it) {
            if (it->second.size() != 0) {
                std::cerr << "Terminal " << (++tcount) << std::endl;

                uint64_t pseed = it->first;
                std::map<uint64_t, std::map<coord16_t, uint64_t> > molecost;
                std::vector<std::vector<GliderRecipe> > strata(maxdepth + 1);
                std::map<uint64_t, std::vector<GliderRecipe> > molecules;

                GliderRecipe gr;
                gr.terminal = false;
                gr.idx_a = pseed;
                gr.idx_b = pseed;
                gr.transpose = 0;
                gr.age = 0;
                gr.dx = 0;
                gr.dy = 0;
                gr.good = 1;
                coord16_t nullcoords(std::pair<int16_t, int16_t>(0, 0), 0);

                molecost[pseed].emplace(nullcoords, 0);
                strata[0].push_back(gr);

                std::cerr << " -- assembling..." << std::endl;
                for (uint64_t d = 1; d <= maxdepth; d++) {
                    for (uint64_t e = 0; e < d; e++) {
                        #pragma omp parallel num_threads(threadcount)
                        {

                            std::map<uint64_t, std::map<coord16_t, GliderRecipe> > molocal;

                            #pragma omp for
                            for (uint64_t i = 0; i < strata[e].size(); i++) {
                                GliderRecipe grx = strata[e][i];
                                uint64_t l = d - e;
                                uint64_t idx_b = grx.idx_b;
                                auto it2 = glob.atomics.find(idx_b);
                                if (it2 != glob.atomics.end()) {
                                    for (auto it3 = it2->second.begin(); it3 != it2->second.end(); ++it3) {
                                        uint64_t idx_c = it3->first;
                                        for (uint64_t j = 0; j < it3->second.size(); j++) {
                                            if ((it3->second[j].length() == l) && (it3->second[j].good != 2)) {
                                                GliderRecipe gry = grx + (it3->second[j]);
                                                coord16_t finish(std::pair<int16_t, int16_t>(gry.dx, gry.dy), gry.transpose);
                                                auto it4 = molecost.find(idx_c);
                                                if ((it4 == molecost.end()) || (it4->second.find(finish) == it4->second.end())) {
                                                    molocal[idx_c].emplace(finish, gry);
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            #pragma omp critical
                            {
                                for (auto it2 = molocal.begin(); it2 != molocal.end(); ++it2) {
                                    uint64_t idx_c = it2->first;
                                    for (auto it3 = it2->second.begin(); it3 != it2->second.end(); ++it3) {
                                        coord16_t finish = it3->first;
                                        if (molecost[idx_c].find(finish) == molecost[idx_c].end()) {
                                            strata[d].push_back(it3->second);
                                            molecules[idx_c].push_back(it3->second);
                                            molecost[idx_c].emplace(finish, d);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    std::cerr << "  -- iterated up to depth " << d << "." << std::endl;
                }

                std::cerr << " -- printing..." << std::endl;
                glob.summarise(pseed, "Source");
                for (auto it2 = molecules.begin(); it2 != molecules.end(); ++it2) {
                    if (it2->second.size() != 0) {
                        glob.summarise(it2->first, "Target");
                        for (auto it3 = it2->second.begin(); it3 != it2->second.end(); ++it3) {
                            it3->printseq();
                        }
                    }
                }
            }
        }
    }

    void parallel_traverse(uint64_t traversals) {

        std::set<uint64_t> terminals;
        std::map<uint64_t, std::string> diterms;

        for (uint64_t i = 0; i < glob.targets.size(); i++) {
            std::string tstring = glob.targets[i].first;
            tstring = tstring.substr(tstring.find(' '));
            if ((tstring[tstring.size() - 3] == '{') && (tstring[tstring.size() - 1] == '}')) {
                int x = (tstring[tstring.size() - 2] - '0');
                if (x <= diterm_thresh) {
                    if ((diterm_filter == "none") || (tstring.find(diterm_filter) != std::string::npos)) {
                        diterms.emplace(i, "");
                    }
                }
                if (x <= term_thresh) {
                    if ((term_filter == "none") || (tstring.find(term_filter) != std::string::npos)) {
                        terminals.insert(i);
                        if (diterms.find(i) == diterms.end()) { diterms.emplace(i, ""); }
                    }
                }
            }
        }

        if (diterm_thresh == -1) {
            std::cerr << "Using xwss seeds as diterminals." << std::endl;
            for (uint64_t i = 0; i < glob.xwssgen.size(); i++) {
                std::string xrec = glob.xwssgen[i];
                uint64_t s = std::stoll(xrec.substr(0, xrec.find(' ')));
                std::string xrec2 = xrec.substr(xrec.find(' ') + 1);
                diterms.emplace(s, xrec2.substr(0, xrec2.find(' ')));
            }
        }

        std::cerr << terminals.size() << " terminals and ";
        std::cerr << diterms.size() << " diterminals." << std::endl;
        uint64_t tcount = 0;

        for (auto it = terminals.begin(); it != terminals.end(); ++it) {
            std::cerr << "Terminal " << (++tcount) << std::endl;
            Traversal trav(&glob, &tlib);
            uint64_t s = *it;
            trav.addstart(s);

            std::cerr << " -- traversing..." << std::endl;
            for (uint64_t depth = 1; depth <= traversals; depth++) {
                bool show_progress = (depth >= (traversals - 1));
                if (depth == traversals) {
                    trav.iterate_full(depth, threadcount, &terminals, show_progress, &diterms);
                } else {
                    trav.iterate_full(depth, threadcount, &terminals, show_progress, 0);
                }
            }

            std::cerr << " -- probing...";
            uint64_t exrep = 0;
            std::vector<std::pair<uint64_t, std::string> > dtvector;
            for (auto it2 = diterms.begin(); it2 != diterms.end(); ++it2) {
                // We need to do this in order to pre-create all map containers:
                exrep += glob.atomics[s][it2->first].size();
                dtvector.push_back(std::pair<uint64_t, std::string>(it2->first, it2->second));
            }
            std::cerr << " " << exrep << " existing recipes found." << std::endl;

            std::cerr << " -- storing..." << std::endl;
            #pragma omp parallel num_threads(threadcount)
            {
                HoneyThread loc(threadmem, rulestring);

                #pragma omp for
                for (uint64_t i = 0; i < dtvector.size(); i++) {
                    uint64_t t = dtvector[i].first;
                    std::string malarkey = dtvector[i].second;
                    trav.store_trav(2*t,   malarkey, (safety ? (&loc) : 0), timelimit);
                    trav.store_trav(2*t+1, malarkey, (safety ? (&loc) : 0), timelimit);
                }
            }
        }
    }

    void bombard_once(uint64_t proportion) {

        uint64_t gsize = glob.targets.size();
        std::cerr << "\033[33;1m***** Examining " << (gsize - elapsed);
        std::cerr << " additional targets *****\033[0m" << std::endl;

        #pragma omp parallel num_threads(threadcount)
        {
            HoneyThread loc(threadmem, rulestring);

            #pragma omp for
            for (uint64_t i = elapsed; i < gsize; i++) {
                uint64_t residue = 0;
                if (proportion >= 2) {
                    std::ostringstream ss;
                    ss << i << localseed;
                    std::string hash = sha256(ss.str());
                    for (int j = 0; j < 16; j++) {
                        char c = hash[j];
                        int hexdigit = 0;
                        if ((c >= '0') && (c <= '9')) { hexdigit = c - '0'; }
                        if ((c >= 'a') && (c <= 'f')) { hexdigit = (c - 'a') + 10; }
                        residue = (residue << 4) + hexdigit;
                    }
                    residue = residue % proportion;
                }
                if (residue == 0) {
                    loc.collidewith(glob.targets[i], i, &(glob), innersize, outersize, iflags, &borings, timelimit);
                }
            }

            #pragma omp critical
            {
                loc.globalise(&glob, tlib);
            }
        }

        elapsed = gsize;

        std::cerr << "\033[33;1m***** Exhausted up to " << (++glex) << " gliders after ";
        std::cerr << gsize << " targets *****\033[0m" << std::endl;
    }


    void parseline(std::string line) {

        if (line[0] == '#') {
            // Comment line:
            std::cerr << "\033[31;1m" << line << "\033[0m" << std::endl;
        } else {
            // Command line:
            std::string command = line.substr(0, line.find(' '));
            std::string periphery = line.substr(line.find(' ')+1);
            std::cerr << "\033[32;1m" << command << "\033[0m " << periphery << std::endl;
            if (command == "include") {
                hsearch_file(periphery);
            } else if (command == "key") {
                payoshakey = periphery;
            } else if (command == "forget") {
                if (periphery == "census") { glob.census.clear(); }
                if (periphery == "alloccur") { glob.alloccur.clear(); }
            } else if (command == "setrule") {
                setrule(periphery);
            } else if (command == "addseed") {
                glob.addseed(periphery);
            } else if (command == "blockseed") {
                glob.blockseed(periphery, innersize);
            } else if (command == "safety") {
                if (periphery == "on") { safety = 1; }
                if (periphery == "off") { safety = 0; }
            } else if (command == "boring") {
                borings[periphery] = 1;
            } else if (command == "interesting") {
                borings[periphery] = 0;
            } else if (command == "exciting") {
                borings[periphery] = -1;
            } else if (command == "term") {
                term_thresh = std::stoll(periphery);
            } else if (command == "diterm") {
                diterm_thresh = std::stoll(periphery);
            } else if (command == "termfilter") {
                term_filter = periphery;
            } else if (command == "ditermfilter") {
                diterm_filter = periphery;
            } else if (command == "threadmem") {
                threadmem = std::stoll(periphery);
            } else if (command == "threadcount") {
                threadcount = std::stoll(periphery);
            } else if (command == "timelimit") {
                timelimit = std::stoll(periphery);
            } else if (command == "iflags") {
                iflags = std::stoll(periphery);
            } else if (command == "innersize") {
                innersize = std::stoll(periphery);
            } else if (command == "outersize") {
                outersize = std::stoll(periphery);
            } else if (command == "traverse") {
                uint64_t traversals = std::stoll(periphery);
                parallel_traverse(traversals);
            } else if (command == "assemble") {
                uint64_t molesize = std::stoll(periphery);
                make_molecules(molesize);
            } else if (command == "bombard") {
                int ng = std::stoll(periphery);
                for (int j = 0; j < ng; j++) { bombard_once(0); }
            } else if (command == "random") {
                bombard_once(std::stoll(periphery));
            } else if (command == "upload") {
                glob.submitResults(payoshakey, localseed);
            } else if (command == "print") {
                if (periphery == "cat") {
                    std::cout << glob.getResults(localseed);
                } else if (periphery == "xwss") {
                    for (uint64_t i = 0; i < glob.xwssgen.size(); i++) {
                        std::cout << glob.xwssgen[i] << std::endl;
                    }
                } else if (periphery == "minimal") {
                    for (auto it3 = glob.minimals.begin(); it3 != glob.minimals.end(); ++it3) {
                        std::cout << "MINIMAL [" << it3->idx_a << " --> " << it3->idx_b << "] : ";
                        if (safety) { glob.verify(*it3, &glob, timelimit); }
                        it3->printseq();
                    }
                } else if (periphery == "atomic") {
                    for (auto it = glob.atomics.begin(); it != glob.atomics.end(); ++it) {
                        uint64_t s = it->first;
                        glob.summarise(s, "Source");
                        for (auto it2 = it->second.begin(); it2 != it->second.end(); ++it2) {
                            uint64_t t = it2->first;
                            if (it2->second.size() != 0) {
                                glob.summarise(t, "Target");
                                for (auto it3 = it2->second.begin(); it3 != it2->second.end(); ++it3) {
                                    it3->printseq();
                                }
                            }
                        }
                    }
                } else {
                    uint64_t upto = tlib.size();
                    if (periphery != "all") {
                        uint64_t upto_new = printed + std::stoll(periphery);
                        upto = (upto < upto_new) ? upto : upto_new;
                    }
                    for (uint64_t i = printed; i < upto; i++) {
                        glob.summarise(i, "Target");
                        for (uint64_t j = 0; j < tlib[i].size(); j++) {
                            glob.printtrans(tlib[i][j]);
                        }
                        std::cout << std::endl;
                    }
                    printed = upto;
                }
            } else {
                std::cerr << "Unrecognised command: \033[31;1m" << command << "\033[0m" << std::endl;
            }
        }
    }

    void hsearch(std::istream &instream) {

        std::string line;

        while (std::getline(instream, line)) {
            if (line.empty()) { continue; }
            if ((line == "return") || (line == "exit") || (line == "quit")) { return; }
            parseline(line);
        }
    }

};


int main(int argc, char *argv[]) {

    if (argc != 2) {
        std::cerr << "Usage: ./hs examples/simple.hsc" << std::endl;
        return 1;
    }

    HoneySearch hsmain(100, 1, "b3s23");

    std::string filename = argv[1];
    hsmain.hsearch_file(filename);

    return 0;

}

