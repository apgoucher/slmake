#!/usr/bin/python

'''
Converts a pattern in any non-B0 isotropic cellular automaton into an
equivalent 'metafied' pattern in B3/S23. This inspects the rulestring
in the provided pattern file to determine how to program the metacell:

Usage: python isotropic_metafier.py infile.rle outfile.mc [outfile.rule]

If outfile.rule is specified, this will additionally emit a Golly
ruletable file implementing the 8-state 4-neighbour automaton used by
the metacell to emulate the desired isotropic rule.

The original announcement of the metacell is here:

https://cp4space.wordpress.com/2018/11/12/fully-self-directed-replication/

The code below uses the Python bindings of lifelib. Due to the way Python
constructs sys.path, it will use the lifelib submodule directory within
this slmake repository, even if you have another installation of lifelib
on your machine.
'''

try:
    from lifelib.genera.isotropic import str2tab
except ImportError:
    print("Could not import str2tab; updating lifelib...")
    import os
    os.system("bash update-lifelib.sh")
    from lifelib.genera.isotropic import str2tab

import lifelib

def rule2tuples(rulestring):
    '''
    Convert an isotropic rulestring to a list of transitions.
    This uses the function str2tab from the isotropic code in
    lifelib.
    '''

    rulestring = rulestring.replace('B', 'b').replace('/S', 's')
    tt = str2tab(rulestring)

    '''
    The following 16-tuple gives a way to colour the 16 different
    2x2 matrices over {0, 1} with 7 labels, such that all 512
    different 3x3 matrices are determined uniquely by the labels
    of the four (overlapping) 2x2 corners.

    The program metacell/chromacube.cpp performs an exhaustive
    search and finds 200 such colourings (up to permutation of
    the labels). The colouring below is the lexicographically
    last amongst those 200:
    '''
    magic_code = (0, 1, 2, 3, 4, 5, 6, 4, 6, 4, 3, 0, 1, 6, 5, 2)

    sq4 = lambda x : magic_code[(x & 3) | ((x >> 1) & 12)]
    sq9 = lambda y : (sq4(y), sq4(y >> 1), sq4(y >> 3), sq4(y >> 4))

    tuples = []

    for i in range(16):
        (a, b, c, d) = (i & 1, (i >> 1) & 1, (i >> 2) & 1, (i >> 3) & 1)
        tuples.append((7*a, 7*b, 7*d, 7*c, magic_code[i]))

    for i in range(512):
        (a, b, c, d) = sq9(i)
        tuples.append((a, b, d, c, 7 * tt[i]))

    return tuples


def tuples2vn(tuples, filename):
    '''
    Function to optionally save a set of transitions as a Golly ruletable.
    '''

    from os.path import splitext, basename

    rulename = splitext(basename(filename))[0]
    rulename = rulename[0].upper() + rulename[1:]

    with open(filename, 'w') as f:

        f.write('@RULE %s\n' % rulename)
        f.write('@TABLE\nn_states:8\nneighborhood:vonNeumann\nsymmetries:none\n\n')

        for t in tuples:
            f.write('0%d%d%d%d%d\n' % t)

        for i in range(8):
            f.write('%d00000\n' % i)


def tuples2tape(tuples):
    '''
    Convert a set of transitions into the tape header for the metacell.
    The output is a list of spacings between successive gliders.
    '''

    tape = [0] + ([90]*41)

    # Amount of time to wait between finishing the 42-glider salute and
    # beginning the first entry of the transition table:
    debt = 703200

    table = ([0] * 4096)

    for (n, e, s, w, x) in tuples:
        table[(e << 9) + (w << 6) + (n << 3) + s] = x

    # Physical commonsense means that 0000 can never occur in practice,
    # except for the 'eden cells' created at the beginning of time. We
    # thus use this entry to represent the initial state that the cell
    # should start in.
    table[0] = 7

    # Transition 7777 is at the front of the tape, and 0000 is at the
    # back, so we need to reverse-iterate over the transition table:
    for j in table[::-1]:

        if (j != 0):
            l = [debt]
            debt = 0
            for _ in range(j - 1):
                l += [90]
                debt -= 90
            tape += l

        debt += 1024

    tape += [debt + 10000]
    return tape


def main(input_file, output_file, output_table=None):

    # The final result will be in b3s23:
    sess = lifelib.load_rules('b3s23')
    lt = sess.lifetree(n_layers=1)

    # Load the target pattern into lifelib and obtain the isotropic rule:
    pattern_to_metafy = lt.load(input_file)
    original_rule = pattern_to_metafy.getrule()

    # Cast to b3s23:
    pattern_to_metafy = lt.pattern("", "b3s23") + pattern_to_metafy

    # Convert the transition table into a tape header for the metacell:
    rtuples = rule2tuples(original_rule)
    tape = tuples2tape(rtuples)
    print(tape)

    # Optionally save a Golly-compatible ruletable:
    if output_table is not None:
        tuples2vn(rtuples, output_table)

    # Load an empty metacell with a glider pointing into its input:
    empty_cell = lt.load("metacell/emptycell.mc")

    # Convert the tape header from a sequence of numbers into an actual
    # stream of gliders heading into the metacell:
    cell_without_glider = empty_cell & empty_cell[8]
    glider = empty_cell - cell_without_glider
    cell_with_tape = cell_without_glider + glider.stream(tape)

    # Run in HashLife to pipe the tape header into the metacell:
    gensleft = 430531840
    while (gensleft > 16777216):
        cell_with_tape = cell_with_tape[16777216]
        gensleft -= 16777216
        print('%d generations remaining...' % gensleft)
    cell_with_tape = cell_with_tape[gensleft]

    # Load the metacell with full tape and default header:
    print('Loading patterns...')
    default_cell = lt.load("metacell/tableonly_default.mc")
    default_cell_full = lt.load("metacell/complete_default.mc")

    # Use a 3-way XOR to obtain a metacell with full tape and the
    # correct header for the desired rule:
    print('Applying patch...')
    onecell = (cell_with_tape ^ default_cell) ^ default_cell_full
    onecell = onecell.centre()

    # Multiplication is overloaded to mean Kronecker product:
    metafied_pattern = pattern_to_metafy * onecell

    # Save the metafied pattern:
    metafied_pattern.write_file(output_file)
    print('...saved output in %s' % output_file)


if __name__ == '__main__':

    from sys import argv
    if (len(argv) != 3):
        raise TypeError("Usage: python isotropic_metafier.py infile.rle outfile.mc [outfile.rule]")

    main(*(argv[1:]))

